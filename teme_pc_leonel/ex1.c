#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum boolean{
	FALSE = 0,
	TRUE = 1
} Boolean;

void read_tree_height(unsigned int* const position,unsigned int* const value,const unsigned long* const N ){
	
	//daca citirea se face eronata value va ramane 0 ceea ce indica finalul sirului sau o alta problema
	*value = 0;
	if(*position < *N){
		scanf("%d",value);
		++(*position);
	}

};

//functie ce actualizeaza datele in cazul in care se gaseste un copac special
void process_special_tree(const unsigned int* const tree_height,const unsigned int* const current_position,unsigned long* const suma,float* const m_a,unsigned int* const xmax_impar,unsigned int* const xmax_par){
	static int special_trees = 0;
	++special_trees;
	*suma += *tree_height;
	*m_a = ((float)*suma)/special_trees;
	//este actualizata valoarea pentru maximul pozitiilor impare
	if((*current_position % 2) == 1)
		if(*tree_height > *xmax_impar)
		*xmax_impar = *tree_height;
	//este actualizata valoarea pentru maximul pozitiilor pare
	if((*current_position % 2) == 0)
		if(*tree_height > *xmax_par)
			*xmax_par = *tree_height;
	
	special_trees++;
}

Boolean is_tree_half_special_left(const unsigned int* const left_tree_height,const unsigned int* const middle_tree_height){
	
	if( *left_tree_height < *middle_tree_height)
		return TRUE;
	return FALSE;
}
Boolean is_tree_half_special_right(const unsigned int* const middle_tree_height,const unsigned int* const right_tree_height){
	
	if( *right_tree_height < *middle_tree_height)
		return TRUE;
	return FALSE;
}

int main(int argc,char** args){

	unsigned long N;
	unsigned long suma = 0;
	float media_aritmetica = 0;
	unsigned int inaltime_maxima_indice_impar = 0;
	unsigned int inaltime_maxima_indice_par = 0;
	unsigned int left_tree_height = 0;
	unsigned int right_tree_height = 0;
	unsigned int current_tree_height = 0;
	unsigned int trees_read = 0;
	// flag that indicates that a tree in the right position was read
	// when the flag is TRUE the read si skipped
	Boolean skip_one_read = FALSE;
	Boolean is_tree_special = FALSE;
	scanf("%lu",&N);
	do{
		if(skip_one_read == TRUE)
			skip_one_read = FALSE;
		else
			read_tree_height(&trees_read,&current_tree_height,&N);

		if(current_tree_height == 0)
			break;
		if(trees_read == 1){
			left_tree_height = current_tree_height;
			continue;
		}
		//daca copacul din stanga ofera caracteristica de copac special celui curent se continuia cu citirea copacului din dreapta
		if(is_tree_half_special_left(&left_tree_height,&current_tree_height) == TRUE){
			read_tree_height(&trees_read,&right_tree_height,&N);
			if(right_tree_height == 0)
				//daca copacul din dreapta nu exista (au fost cititi toti copacii atunci se iese din bucla)
				break;
			is_tree_special = is_tree_half_special_right(&current_tree_height,&right_tree_height);
			if(is_tree_special){
				//daca copacul este unul special atunci sunt actualizate informatiile de statistica
				//cel din dreapta va fi considerat ca fiind copac de stanga intrucat nu are rost ca acesta sa fie verificat ca fiind
				//copac special din moment ce inaltimea lui este mai mica decat cea a copacului din stanga sa
				process_special_tree(&current_tree_height,&trees_read,&suma,&media_aritmetica,&inaltime_maxima_indice_impar,&inaltime_maxima_indice_par);
				left_tree_height = right_tree_height;
			}
			else{
				//daca copacul din dreapta celui curent a fost citit dar nu are inaltimea mai mica decat cel current
				//atunci copacul din dreapta va fi considerat ca fiind un posibil copac special
				//se activeaza si flag-ul care va omite o citire
				left_tree_height = current_tree_height;
				current_tree_height = right_tree_height;
				skip_one_read=TRUE;
			}
		}
		else{
			//daca copacul din stanga nu e mai mic decat cel din mijloc cel din urma va fi considerat ca fiind copac de stanga
			//nu se activeaza flag-ul ca la linia 100 deoarece copacul din dreapta nu a fost citit inca
			left_tree_height = current_tree_height;
		}
	}while(current_tree_height != 0);

	printf("%ld\n%0.7f\n%d\n%d",suma,media_aritmetica,inaltime_maxima_indice_impar,inaltime_maxima_indice_par);

	return 0;
}