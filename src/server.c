#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include "common_types.h"

#define PORT 8080

int main(int argc,char const* argv[]){

    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char* hello = "hello from server";

    if((server_fd = socket(AF_INET,SOCK_STREAM,0)) == 0){
        perror("socket_failed");
        exit(EXIT_FAILURE);
    }
    
    if(setsockopt(server_fd,SOL_SOCKET,SO_REUSEADDR | SO_REUSEPORT, &opt,sizeof(opt))){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if(bind(server_fd, (struct sockaddr*)&address,sizeof(address)) < 0){
        perror("bind_failed");
        exit(EXIT_FAILURE);
    }

    if(listen(server_fd,3) < 0){
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if((new_socket = accept(server_fd, (struct sockaddr*)&address,(socklen_t*)&addrlen)) < 0){
        perror("accept");
        exit(EXIT_FAILURE);
    }
    while(1){
        memset(buffer,0,1024);
        valread = read(new_socket, buffer, 1024);
        if(strncmp(buffer,"exit",4) == 0){
            printf("\nThe client wanted to exit\n");
            return 0;
        }
        printf("Message from client: %s\n", buffer);
    }
    return 0;
}